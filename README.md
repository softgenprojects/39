
<p align="center">
  <img src="https://run.softgen.ai/images/softgenai.svg" alt="SoftGen.ai Logo" width="200"/>
</p>

<h1 align="center">Your SoftGen.ai Project</h1>

<p align="center">
  Crafted with 🤖 by <a href="http://www.SoftGen.ai">SoftGen.ai</a>.
</p>

## 🚀 Getting Started

Welcome to the project! To spin up your development or production environment simply follow these instructions.

### 🛠 Backend Setup

To ignite the backend server, fire up your terminal and run:

npm i<br>
npm start

### 🌐 Frontend Setup
To get the frontend up and running, open a new terminal window and execute:

#### Development
npm i<br>
npm run dev
<br>
<br>
#### Production
npm i
<br>
npm run build
<br>
npm start